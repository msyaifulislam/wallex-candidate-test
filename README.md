# README #

Test wallex for backend dev.


### System Requirements 

* NodeJS 8
* Mysql/ other database server


### Setup ? ###

* Global configuration on Config/config.js
* DB_DIALECT (mysql), DB_HOST, DB_PORT (if used), DB_NAME, DB_USER, DB_PASSWORD

### Endpoint Available (Routes/v1.js), also can load template postman ###
* Get fibonacci : /api/v1/Fibonacci/:value (return will list prime fibonacci number and the summary)



* Login user (post) /api/v1/users/login (produces token) (also get token from this endpoint)
* Create user (post) /api/v1/users/signup (produces token) (also get token from this endpoint)

### (header : Bearer token)
* Get user info (get) /api/v1//users 
* Update user info (put)     /api/v1/users
* Delete user                /api/v1/users

### restaurant api
* post /api/v1/menu      
* get  /api/v1/menulistall
* get /api/v1/menu/:menu_id
* put /api/v1/menu/:menu_id
* delete /api/v1/menu/:menu_id

* post /api/v1/order/addOrder (add order restaurant: body (menu_id, total_menu)


