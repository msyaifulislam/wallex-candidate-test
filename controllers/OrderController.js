const Order = require('../models').Order;
const OrderDetail = require('../models').OrderDetail
const Menu = require('../models').Menu

const create = async function(req, res){
    res.setHeader('Content-Type', 'application/json');

    let err, order;
    let user = req.user;

    let order_info = req.body;


    [err, order] = await to(Order.create(order_info));
    if(err) return ReE(res, err, 422);

    order.addUser(user, { through: { status: 'started' }})

    [err, order] = await to(order.save());
    if(err) return ReE(res, err, 422);

    let order_json = order.toWeb();
    order_json.users = user.id;
    order_json.total = 0;

    // console.log(res.body);



    var objs= []
    var id = []
    var total = []
    var menuname=[]
    var price=[]
    for(var key in req.body.menu_id){
        var a= req.body.menu_id[key]
        var b = req.body.total_menu[key]
        objs.push( { id_order: order.id, id_menu: a, total_item: b});
        id.push(a);
        total.push(b)
    }
    this.ress = res;
    this.objss= objs;
    this.total= total;
    this.menuname = menuname;
    this.price = price;
    Menu.count({
        where: {
            id: id
        }
    }).then(res=>{
        if(res == id.length){
            return Menu.findAll({where: {id:id}});

        }else{
            return ReS(ress,{error:'Menu structure wrong or not found. '}, 400);
        }
        // console.log(res);
    }).then(res =>{
        var total_all = 0;
        for(var i =0; i< res.length;i++){
            total_all += res[i].price * total[i];
            menuname.push(res[i].name)
            price.push(res[i].price)
        }

        order_json.total = total_all;
        return OrderDetail.bulkCreate(
            objss
        );

    }).then(res =>{
        console.log(res);
        // order_json.orderedmenudetail= res
        var a =[]
        for(var i=0 ; i<res.length; i++){
            item_subtotal = res[i].total_item * price[i];
            a.push(
                {
                    id_menu: res[i].id_menu,
                    id_order: res[i].id_order,
                    name: menuname[i],
                    price: price[i],
                    total_item: res[i].total_item,
                    item_subtotal: item_subtotal
                }
            )
        }
        order_json.orderedmenudetail = a


        return ReS(ress, {orders:order_json});
    });


    //
    // order_json.menu = objs;
    // return ReS(res, {orders:order_json});


    // Menu.findAll({
    //   where: {
    //     id: 2
    //   }
    // }) .then(users => {
    //     return ReS(res,{users}, 201);
    // })
    //

    //
    // OrderDetail.bulkCreate(
    //     objs
    // ).then(() => { // Notice: There are no arguments here, as of right now you'll have to...
    //     return ReS(res, {orders:order_json});
    // })
}
module.exports.create = create;

function isIdUnique (id) {
    return Menu.count({ where: { id: id } })
      .then(count => {
        if (count != 0) {
          return false;
        }
        return true;
    });
}

const getAllById = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let user = req.user;
    let err, orders;
    // console.log(user);

    [err, orders] = await to(user.getOrders({include: [ {association: Order.Users} ] }));

    let orders_json =[]
    for( let i in orders){
        let order = orders[i];
        let users =  order.Users;
        let order_info = order.toWeb();
        let users_info = [];
        for (let i in users){
            let user = users[i];
            // let user_info = user.toJSON();
            users_info.push({user:user.id});
        }
        order_info.users = users_info;
        orders_json.push(order_info);
    }

    console.log('c t', orders_json);
    return ReS(res, {orders:orders_json});
}
module.exports.getAllById = getAllById;
