const express 			= require('express');
const router 			= express.Router();

const UserController 	= require('./../controllers/UserController');
const MenuController = require('./../controllers/MenuController');
// const HomeController = require('./../controllers/HomeController');
// const HomeController 	= require('./../controllers/HomeController');
const OrderController 	= require('./../controllers/OrderController');

const custom 	        = require('./../middleware/custom');

const passport      	= require('passport');
const path              = require('path');


require('./../middleware/passport')(passport)
/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({status:"success", message:"Parcel Pending API", data:{"version_number":"v1.0.0"}})
});

router.get('/Fibonacci/:val', function(req, res, next) {
    param = req.params.val

    var i=1;

    var max=param;
    var fibo = [];
    var y=0;
    var q=0;
    var sum_prime = 0;
    var prime= []
    while(i < max){
    	if(i < 2){
    		fibo[y] = i;
    		i++;
    	}
    	else if(i == 2 && y < 2){
    		fibo[y] = fibo[y-1] + 0;
    		i++;
    	}
    	else{
    		fibo[y] = fibo[y-2] + fibo[y-1];
    		i=fibo[y];
    	}
    	if(fibo[y] < max){
    		var prime_word = '';
    		if(test_prime(fibo[y]) == true){
    			q++;
    			// prime_word = 'is prime number';
                var j= fibo[y]
                prime.push(j)
    			sum_prime = sum_prime + fibo[y];
    		}
    	}
    	y++;

    }
    res.json({status:"success", message:"Prime Fibonaci: "+ prime+ " & Summary prime number from fibonaci is " + sum_prime})
});

function test_prime(n)
{
	if (n===1 || n ===0)
	{
		return false;
	}
	else if(n === 2)
	{
		return true;
	}else{
		for(var x = 2; x < n; x++)
		{
		  if(n % x === 0)
		  {
		    return false;
		  }
		}
	return true;
	}
}




router.post(    '/users/signup',    UserController.create);                                                    // C
router.get(     '/users',           passport.authenticate('jwt', {session:false}), UserController.get);        // R
router.put(     '/users',           passport.authenticate('jwt', {session:false}), UserController.update);     // U
router.delete(  '/users',           passport.authenticate('jwt', {session:false}), UserController.remove);     // D
router.post(    '/users/login',     UserController.login);

router.post(    '/menu',             passport.authenticate('jwt', {session:false}), MenuController.create);                  // C
router.get(     '/menulistall',      passport.authenticate('jwt', {session:false}), MenuController.getAll);                  // R

router.get(     '/menu/:menu_id', passport.authenticate('jwt', {session:false}), custom.menu, MenuController.get);     // R
router.put(     '/menu/:menu_id', passport.authenticate('jwt', {session:false}), custom.menu, MenuController.update);  // U
router.delete(  '/menu/:menu_id', passport.authenticate('jwt', {session:false}), custom.menu, MenuController.remove);  // D

router.post( '/order/addOrder', passport.authenticate('jwt', {session:false}), OrderController.create);
router.get(  '/orderlist',   passport.authenticate('jwt', {session:false}), OrderController.getAllById);                  // R

// router.get('/dash', passport.authenticate('jwt', {session:false}),HomeController.Dashboard)


//********* API DOCUMENTATION **********
// router.use('/docs/api.json',            express.static(path.join(__dirname, '/../public/v1/documentation/api.json')));
// router.use('/docs',                     express.static(path.join(__dirname, '/../public/v1/documentation/dist')));
module.exports = router;
