'use strict';
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('Menu', {
    name: DataTypes.STRING,
    price: DataTypes.DECIMAL
  });

  Model.associate = function(models){
      this.Users = this.belongsToMany(models.User, {through: 'UserMenu'});
  };


  Model.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };

  return Model;
};
